package stockApi

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"

	"github.com/tidwall/gjson"
)

const Metadata = "Meta Data"
const LastRefreshed = "3. Last Refreshed"
const Url = "https://www.alphavantage.co/query?function=%s&symbol=%s&interval=%s&apikey=%s"
const TimeInterval = "Time Series (%s)"

type StockValue struct {
	Low       string
	High      string
	Timestamp string
}

func GetStock(key string, symbol string) StockValue {
	//key := "DKET7CF9WW3K4IKR"
	//symbol := "GME"
	resource := "TIME_SERIES_INTRADAY"
	interval := "1min"
	highValue, err := ExtractLatest(key, symbol, resource, interval)
	if err != nil {
		log.Fatal("NewRequest: ", err)
	}
	return highValue

}

func ExtractLatest(key string, symbol string, resource string, interval string) (StockValue, error) {
	body, err := Get(key, symbol, resource, interval)
	if err == nil {
		md := gjson.Get(body, Metadata)
		log.Print(body)
		field := Parse(md.String(), 3, 1)
		intervalField := fmt.Sprintf(TimeInterval, interval)
		path := strings.Join([]string{intervalField, field}, ".")
		result := gjson.Get(body, path)
		high := Parse(result.String(), 3, 1)
		low := Parse(result.String(), 2, 1)
		return StockValue{low, high, field}, nil
	}

	return StockValue{
		Low:       "",
		High:      "",
		Timestamp: "",
	}, err
}

func Parse(md string, line int, match int) string {
	lines := strings.Split(md, "\n")
	re := regexp.MustCompile(`"(.*?)"`)
	field := re.FindAllString(strings.Trim(lines[line], "\""), 2)
	return field[match][1 : len(field[match])-1]
}

func Get(key string, symbol string, resource string, interval string) (string, error) {

	url := fmt.Sprintf(Url, resource, symbol, interval, key)
	log.Print(Url)
	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
	}

	if resp.Body != nil {
		defer resp.Body.Close()
	}

	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	return string(body), err
}
